#include<iostream>
#include<string>
#include<opencv2/core/core.hpp>
#include<opencv2/opencv.hpp>
#include<opencv2/highgui/highgui.hpp>
using namespace std;
using namespace cv;
/**
 \mainpage
Le programme record permet d'enregistrer un video de 5 secondes dans un fichier "capture-liv1.avi".
Lors de l'appel du programme, l'usager doit entrer la resolution en X et en Y des frames MJPG, le nombre d'images par secondes et finalement la duree de l'enregistrement.
Voici un exemple de l'appel du programme Record : 

	./Record 1280 720 8.52 5

Pour un enregistrement 1280x720 avec un fps de 8.52 pour un enregistrement de 5 secondes.

**/
int main(int argc, char **argv)
{
	int resX = atoi(argv[1]);
	int resY = atoi(argv[2]);
	float fps = atof(argv[3]);
	int duration = atoi(argv[4]);
	int frames = (int)(fps*duration);

	if(argc != 5)
	{
		cout << "Not enough arguments..." << endl;
		return -1;
	}

	cout << "Camera's Resolution = " << resX << "x" << resY << endl;
	cout << "FPS = " << fps << endl;
	cout << "Number of frames that will be acquired = " << frames << endl;
	
	VideoCapture capture(0);
	capture.set(CV_CAP_PROP_FRAME_WIDTH,resX);
	capture.set(CV_CAP_PROP_FRAME_HEIGHT,resY);
	if(!capture.isOpened()){
		cout << "Failed to connect to the camera." << endl;
	}
	
	Mat frame;

	VideoWriter video("capture-liv1.avi",CV_FOURCC('M','J','P','G'),fps,Size(resX,resY));
	for(int i=0; i<frames; i++){
		capture >> frame;
		if(frame.empty()){
			cout << "Failed to capture an image" << endl;
			return -1;
		}
		video.write(frame);
	}
	capture.release();
	video.release();
	cout << "Video Recorded !" << endl;
	return 0;
}