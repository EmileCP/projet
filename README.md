## Livrable 1 Description

Le programme record permet d'enregistrer un video de 5 secondes dans un fichier "capture-liv1.avi".
Lors de l'appel du programme, l'usager doit entrer la resolution en X et en Y des frames MJPG, le nombre d'images par secondes et finalement la duree de l'enregistrement.
Voici un exemple de l'appel du programme Record : 

	./Record 1280 720 8.52 5

Pour un enregistrement 1280x720 avec un fps de 8.52 pour un enregistrement de 5 secondes.
